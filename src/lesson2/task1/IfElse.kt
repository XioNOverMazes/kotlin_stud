@file:Suppress("UNUSED_PARAMETER")

package lesson2.task1

import lesson1.task1.discriminant
import lesson1.task1.sqr

/**
 * Пример
 *
 * Найти наименьший корень биквадратного уравнения ax^4 + bx^2 + c = 0
 */
fun minBiRoot(a: Double, b: Double, c: Double): Double {
    // 1: в главной ветке if выполняется НЕСКОЛЬКО операторов
    if (a == 0.0) {
        if (b == 0.0) return Double.NaN // ... и ничего больше не делать
        val bc = -c / b
        if (bc < 0.0) return Double.NaN // ... и ничего больше не делать
        return -Math.sqrt(bc)
        // Дальше функция при a == 0.0 не идёт
    }
    val d = discriminant(a, b, c)   // 2
    if (d < 0.0) return Double.NaN  // 3
    // 4
    val y1 = (-b + Math.sqrt(d)) / (2 * a)
    val y2 = (-b - Math.sqrt(d)) / (2 * a)
    val y3 = Math.max(y1, y2)       // 5
    if (y3 < 0.0) return Double.NaN // 6
    return -Math.sqrt(y3)           // 7
}

/**
 * Простая
 *
 * Мой возраст. Для заданного 0 < n < 200, рассматриваемого как возраст человека,
 * вернуть строку вида: «21 год», «32 года», «12 лет».
 */
fun ageDescription(age: Int): String {
    var strn = age.toString();
    /* 5-20 let */
    if ((age >= 5 && age <= 20) || (age >= 111 && age <= 120)) {
        return (strn + " лет");
    }
    /* "god" */
    var lastch = (strn[strn.length - 1]).toString().toInt();
    if (strn[strn.length - 1] == '1') {
        return strn + " год";
    }
    if (lastch >= 2 && lastch <= 4) {
        return strn + " года";
    } else {
        return strn + " лет";
    }

    return "shit";
}

/**
 * Простая
 *
 * Путник двигался t1 часов со скоростью v1 км/час, затем t2 часов — со скоростью v2 км/час
 * и t3 часов — со скоростью v3 км/час.
 * Определить, за какое время он одолел первую половину пути?
 */
fun timeForHalfWay(t1: Double, v1: Double,
                   t2: Double, v2: Double,
                   t3: Double, v3: Double): Double {
    var halfway: Double = (t1 * v1 + t2 * v2 + t3 * v3) / 2;
    if (t1 * v1 >= halfway) {
        return halfway / v1;
    }
    if (t1 * v1 + t2 * v2 >= halfway) {
        var S1: Double = v1 * t1;
        var S22: Double = halfway - S1;
        var t22: Double = S22 / v2;
        return t1 + t22;
    } else {
        var S1: Double = v1 * t1;
        var S2: Double = v2 * t2;
        var S33 = halfway - (S2 + S1);
        var t33: Double = S33 / v3;
        return t1 + t2 + t33;
    }

}

/**
 * Простая
 *
 * Нa шахматной доске стоят черный король и две белые ладьи (ладья бьет по горизонтали и вертикали).
 * Определить, не находится ли король под боем, а если есть угроза, то от кого именно.
 * Вернуть 0, если угрозы нет, 1, если угроза только от первой ладьи, 2, если только от второй ладьи,
 * и 3, если угроза от обеих ладей.
 * Считать, что ладьи не могут загораживать друг друга
 */
fun whichRookThreatens(kingX: Int, kingY: Int,
                       rookX1: Int, rookY1: Int,
                       rookX2: Int, rookY2: Int): Int {
    if ((kingX == rookX1 || kingY == rookY1) && (kingX == rookX2 || kingY == rookY2)) {
        return 3;
    }
    /* Check first */
    if (kingX == rookX1 ||
            kingY == rookY1) {
        return 1;
    }
    if (kingX == rookX2 ||
            kingY == rookY2) {
        return 2;
    }
    return 0;
}

/**
 * Простая
 *
 * На шахматной доске стоят черный король и белые ладья и слон
 * (ладья бьет по горизонтали и вертикали, слон — по диагоналям).
 * Проверить, есть ли угроза королю и если есть, то от кого именно.
 * Вернуть 0, если угрозы нет, 1, если угроза только от ладьи, 2, если только от слона,
 * и 3, если угроза есть и от ладьи и от слона.
 * Считать, что ладья и слон не могут загораживать друг друга.
 */
fun rookOrBishopThreatens(kingX: Int, kingY: Int,
                          rookX: Int, rookY: Int,
                          bishopX: Int, bishopY: Int): Int {
    var x_l = Math.abs(kingX - bishopX);
    var y_l = Math.abs(kingY - bishopY);
    if ((kingX == rookX || kingY == rookY) && x_l == y_l) {
        return 3;
    }
    if (kingX == rookX || kingY == rookY) {
        return 1;
    }

    if (x_l == y_l) {
        return 2;
    }
    return 0;
}

/**
 * Простая
 *
 * Треугольник задан длинами своих сторон a, b, c.
 * Проверить, является ли данный треугольник остроугольным (вернуть 0),
 * прямоугольным (вернуть 1) или тупоугольным (вернуть 2).
 * Если такой треугольник не существует, вернуть -1.
 */
fun triangleKind(a: Double, b: Double, c: Double): Int {
    if (a + b > c && a + c > b && b + c > a && a > 0 && b > 0 && c > 0) {
        val sds = arrayOf<Double>(a, b, c);
        sds.sort();
        if (sqr(sds[2]) < (sqr(sds[0]) + sqr(sds[1]))) {
            return 0;
        }
        if (sqr(sds[2]) == (sqr(sds[0]) + sqr(sds[1]))) {
            return 1;
        }
        if (sqr(sds[2]) > (sqr(sds[0]) + sqr(sds[1]))) {
            return 2;
        }
    }
    return -1;
}

/**
 * Средняя
 *
 * Даны четыре точки на одной прямой: A, B, C и D.
 * Координаты точек a, b, c, d соответственно, b >= a, d >= c.
 * Найти длину пересечения отрезков AB и CD.
 * Если пересечения нет, вернуть -1.
 */
fun segmentLength(a: Int, b: Int, c: Int, d: Int): Int {
    var r_a: Int = 0;
    var r_b: Int = 0;
    var r_c: Int = 0;
    var r_d: Int = 0;
    /* the more value is on first place */
    if (a <= c) {
        r_a = a;
        r_b = b;
        r_c = c;
        r_d = d;
    } else {
        r_a = c;
        r_b = d;
        r_c = a;
        r_d = b;
    }

    if (r_a == r_c) {
        if (r_d >= r_b) {
            return r_b - r_a;
        } else {
            return r_d - r_a;
        }
    }
    if (r_c > r_a && r_c < r_b) {
        if (r_d < r_b) {
            return r_d - r_c;
        }
        if (r_d > r_b) {
            return r_b - r_c;
        }
    }
    if (r_c == r_b) {
        return 0;
    }
    return -1;
}

fun segmentLengthDouble(a: Double, b: Double, c: Double, d: Double): Double {
    var r_a: Double = 0.0;
    var r_b: Double = 0.0;
    var r_c: Double = 0.0;
    var r_d: Double = 0.0;
    /* the more value is on first place */
    if (a <= c) {
        r_a = a;
        r_b = b;
        r_c = c;
        r_d = d;
    } else {
        r_a = c;
        r_b = d;
        r_c = a;
        r_d = b;
    }

    if (r_a == r_c) {
        if (r_d >= r_b) {
            return r_b - r_a;
        } else {
            return r_d - r_a;
        }
    }
    if (r_c > r_a && r_c < r_b) {
        if (r_d < r_b) {
            return r_d - r_c;
        }
        if (r_d > r_b) {
            return r_b - r_c;
        }
    }
    if (r_c == r_b) {
        return 0.0;
    }
    return -1.0;
}
