@file:Suppress("UNUSED_PARAMETER")

package lesson4.task1

import lesson1.task1.discriminant
import lesson3.task1.digitNumber
import lesson3.task1.getDigitFromNum

/**
 * Пример
 *
 * Найти все корни уравнения x^2 = y
 */
fun sqRoots(y: Double) =
        when {
            y < 0 -> listOf()
            y == 0.0 -> listOf(0.0)
            else -> {
                val root = Math.sqrt(y)
                // Результат!
                listOf(-root, root)
            }
        }

/**
 * Пример
 *
 * Найти все корни биквадратного уравнения ax^4 + bx^2 + c = 0.
 * Вернуть список корней (пустой, если корней нет)
 */
fun biRoots(a: Double, b: Double, c: Double): List<Double> {
    if (a == 0.0) {
        return if (b == 0.0) listOf()
        else sqRoots(-c / b)
    }
    val d = discriminant(a, b, c)
    if (d < 0.0) return listOf()
    if (d == 0.0) return sqRoots(-b / (2 * a))
    val y1 = (-b + Math.sqrt(d)) / (2 * a)
    val y2 = (-b - Math.sqrt(d)) / (2 * a)
    return sqRoots(y1) + sqRoots(y2)
}

/**
 * Пример
 *
 * Выделить в список отрицательные элементы из заданного списка
 */
fun negativeList(list: List<Int>): List<Int> {
    val result = mutableListOf<Int>()
    for (element in list) {
        if (element < 0) {
            result.add(element)
        }
    }
    return result
}

/**
 * Пример
 *
 * Изменить знак для всех положительных элементов списка
 */
fun invertPositives(list: MutableList<Int>) {
    for (i in 0 until list.size) {
        val element = list[i]
        if (element > 0) {
            list[i] = -element
        }
    }
}

/**
 * Пример
 *
 * Из имеющегося списка целых чисел, сформировать список их квадратов
 */
fun squares(list: List<Int>) = list.map { it * it }

/**
 * Пример
 *
 * По заданной строке str определить, является ли она палиндромом.
 * В палиндроме первый символ должен быть равен последнему, второй предпоследнему и т.д.
 * Одни и те же буквы в разном регистре следует считать равными с точки зрения данной задачи.
 * Пробелы не следует принимать во внимание при сравнении символов, например, строка
 * "А роза упала на лапу Азора" является палиндромом.
 */
fun isPalindrome(str: String): Boolean {
    val lowerCase = str.toLowerCase().filter { it != ' ' }
    for (i in 0..lowerCase.length / 2) {
        if (lowerCase[i] != lowerCase[lowerCase.length - i - 1]) return false
    }
    return true
}

/**
 * Пример
 *
 * По имеющемуся списку целых чисел, например [3, 6, 5, 4, 9], построить строку с примером их суммирования:
 * 3 + 6 + 5 + 4 + 9 = 27 в данном случае.
 */
fun buildSumExample(list: List<Int>) = list.joinToString(separator = " + ", postfix = " = ${list.sum()}")

/**
 * Простая
 *
 * Найти модуль заданного вектора, представленного в виде списка v,
 * по формуле abs = sqrt(a1^2 + a2^2 + ... + aN^2).
 * Модуль пустого вектора считать равным 0.0.
 */
fun abs(v: List<Double>): Double {
    var sum: Double = 0.0
    for (element in v) {
        sum += element * element;
    }
    return Math.sqrt(sum)
}

/**
 * Простая
 *
 * Рассчитать среднее арифметическое элементов списка list. Вернуть 0.0, если список пуст
 */
fun mean(list: List<Double>): Double {
    if (list.size == 0) return 0.0;
    var sum: Double = 0.0;
    for (element in list)
        sum += element;
    return sum / list.size;
}

/**
 * Средняя
 *
 * Центрировать заданный список list, уменьшив каждый элемент на среднее арифметическое всех элементов.
 * Если список пуст, не делать ничего. Вернуть изменённый список.
 *
 * Обратите внимание, что данная функция должна изменять содержание списка list, а не его копии.
 */
fun center(list: MutableList<Double>): MutableList<Double> {
    var midari = mean(list);
    for (i in 0 until list.size) {
        list[i] -= midari;
    }
    return list;
}

/**
 * Средняя
 *
 * Найти скалярное произведение двух векторов равной размерности,
 * представленные в виде списков a и b. Скалярное произведение считать по формуле:
 * C = a1b1 + a2b2 + ... + aNbN. Произведение пустых векторов считать равным 0.0.
 */
fun times(a: List<Double>, b: List<Double>): Double {
    var turbosum: Double = 0.0;
    var i: Int = 0;
    var j: Int = 0;
    while (i != a.size - 1) {
        turbosum += a[i] * b[i];
        i++;
        j++;
    }
    return turbosum;
}

/**
 * Средняя
 *
 * Рассчитать значение многочлена при заданном x:
 * p(x) = p0 + p1*x + p2*x^2 + p3*x^3 + ... + pN*x^N.
 * Коэффициенты многочлена заданы списком p: (p0, p1, p2, p3, ..., pN).
 * Значение пустого многочлена равно 0.0 при любом x.
 */
fun polynom(p: List<Double>, x: Double): Double {
    var sum: Double = 0.0;
    for (i in 0 until p.size) {
        sum += p[i] * Math.pow(x, i.toDouble());
    }
    return sum;
}

/**
 * Средняя
 *
 * В заданном списке list каждый элемент, кроме первого, заменить
 * суммой данного элемента и всех предыдущих.
 * Например: 1, 2, 3, 4 -> 1, 3, 6, 10.
 * Пустой список не следует изменять. Вернуть изменённый список.
 *
 * Обратите внимание, что данная функция должна изменять содержание списка list, а не его копии.
 */
fun accumulate(list: MutableList<Double>): MutableList<Double> {
    val retlis = mutableListOf<Double>();
    retlis.add(1.0);
    if (list.size == 0)
        return list;
    if (list.size == 1)
        return list;
    var tmpsum: Double = 0.0;
    for (i in 1 until list.size) {
        tmpsum = 0.0;
        var j: Int = 0;
        while (j <= i) {
            tmpsum += list[j];
            j++;
        }
        retlis.add(tmpsum);
    }
    for (i in 0 until retlis.size) {
        list[i] = retlis[i];
    }
    return list;
}

/**
 * Средняя
 *
 * Разложить заданное натуральное число n > 1 на простые множители.
 * Результат разложения вернуть в виде списка множителей, например 75 -> (3, 5, 5).
 * Множители в списке должны располагаться по возрастанию.
 */
fun factorize(n: Int): List<Int> {
    var l: MutableList<Int> = mutableListOf<Int>();
    var rn = n;
    var div: Int = 2;
    while (rn > 1) {
        while (rn % div == 0) {
            l.add(div);
            rn = rn / div;
        }
        div++;
    }
    return l;
}

/**
 * Сложная
 *
 * Разложить заданное натуральное число n > 1 на простые множители.
 * Результат разложения вернуть в виде строки, например 75 -> 3*5*5
 */
fun factorizeToString(n: Int): String {
    var S = "";
    var listo = factorize(n);
    if (listo.size == 1) {
        S += listo[0].toString();
        return S;
    }
    for (i in 0 until listo.size - 1) {
        S += java.lang.String.valueOf(listo[i]);
        S += "*";
    }
    S += listo[listo.size - 1].toString();
    return S;
}

/**
 * Средняя
 *
 * Перевести заданное целое число n >= 0 в систему счисления с основанием base > 1.
 * Результат перевода вернуть в виде списка цифр в base-ичной системе от старшей к младшей,
 * например: n = 100, base = 4 -> (1, 2, 1, 0) или n = 250, base = 14 -> (1, 3, 12)
 */
fun convert(n: Int, base: Int): List<Int> {
    var l: MutableList<Int> = mutableListOf<Int>();
    var rn: Int = n;
    while (rn > 0) {
        var ost: Int = rn % base;
        rn = rn / base;
        l.add(ost);
    }
    l.reverse();
    return l;

}

/**
 * Сложная
 *
 * Перевести заданное целое число n >= 0 в систему счисления с основанием 1 < base < 37.
 * Результат перевода вернуть в виде строки, цифры более 9 представлять латинскими
 * строчными буквами: 10 -> a, 11 -> b, 12 -> c и так далее.
 * Например: n = 100, base = 4 -> 1210, n = 250, base = 14 -> 13c
PO ASCII Lati
 */
fun convertToString(n: Int, base: Int): String {
    return "error";
/*

var l:List<String> = listOf<String>("0","1","2","3","4","5","6","7",
                                    "8","9","a","b","c","d","e","f");

var l: MutableList<Int> =  mutableListOf<Int>();
var r_n:Int = n;
while (r_n > 0)
{
    var ost:Int = r_n%base;
    r_n = r_n/base;
    l.add(ost);
}
l.reverse();
return "helo";*/
}

/**
 * Средняя
 *
 * Перевести число, представленное списком цифр digits от старшей к младшей,
 * из системы счисления с основанием base в десятичную.
 * Например: digits = (1, 3, 12), base = 14 -> 250
 */
fun decimal(digits: List<Int>, base: Int): Int {
    var num: Int = 0;
    var i: Int = 0;
    var j: Int = digits.size - 1;
    while (i < digits.size) {
        num += (digits[i] * Math.pow(base.toDouble(), j.toDouble())).toInt();
        i++;
        j--;
    }
    return num;
}

/**
 * Сложная
 *
 * Перевести число, представленное цифровой строкой str,
 * из системы счисления с основанием base в десятичную.
 * Цифры более 9 представляются латинскими строчными буквами:
 * 10 -> a, 11 -> b, 12 -> c и так далее.
 * Например: str = "13c", base = 14 -> 250
 */
fun decimalFromString(str: String, base: Int): Int = TODO()

/**
 * Сложная
 *
 * Перевести натуральное число n > 0 в римскую систему.
 * Римские цифры: 1 = I, 4 = IV, 5 = V, 9 = IX, 10 = X, 40 = XL, 50 = L,
 * 90 = XC, 100 = C, 400 = CD, 500 = D, 900 = CM, 1000 = M.
 * Например: 23 = XXIII, 44 = XLIV, 100 = C
 */
fun roman(n: Int): String {
    var S = "";
    var lrim = listOf<String>("M", "CM", "D", "CD", "C", "XC",
            "L", "XL", "X", "IX", "V", "IV", "I", " ");
    var lara = listOf<Int>(1000, 900, 500, 400, 100,
            90, 50, 40, 10, 9, 5, 4, 1, 0);
    var rn = n;
    var i: Int = 0;
    while (rn > 0) {
        while (lara[i] <= rn) {
            S += lrim[i];
            rn -= lara[i];
        }
        i += 1;
    }
    return S;
}

/**
 * Очень сложная
 *
 * Записать заданное натуральное число 1..999999 прописью по-русски.
 * Например, 375 = "триста семьдесят пять",
 * 23964 = "двадцать три тысячи девятьсот шестьдесят четыре"
 */

fun makeOneDigitStr(n: Int, flag: Boolean): String {
    var S = "";
    var N = n;
    var ll3s: MutableList<String> = mutableListOf<String>("", "один", "два", "три",
            "четыре", "пять", "шесть",
            "семь", "восемь", "девять");
    if (flag) {
        ll3s[1] = "одна";
        ll3s[2] = "две";
    }
    while (N > 0) {
        var osto = N % 10;
        if (digitNumber(N) == 1) {
            S = ll3s[osto] + S;
        }
        N = N / 10;
    }
    return S;
}

fun makeTwoDigitsStr(n: Int, flag: Boolean): String {
    var S = "";
    var N = n;
    var ll2s: MutableList<String> = mutableListOf<String>("", "десять", "двадцать", "тридцать",
            "сорок", "пятьдесят", "шестьдесят",
            "семьдесят", "восемьдесят", "девяносто");
    var ll3s: MutableList<String> = mutableListOf<String>("", "один", "два", "три",
            "четыре", "пять", "шесть",
            "семь", "восемь", "девять",
            "", "одиннадцать", "двенадцать", "тринадцать",
            "четырнадцать", "пятнадцать", "шестнадцать",
            "семнадцать", "восемьнадцать", "девятнадцать");
    if (flag) {
        ll3s[1] = "одна";
        ll3s[2] = "две"
    }
    if (n > 10 && n < 20) {
        return ll3s[n];
    }
    while (N > 0) {
        var osto = N % 10;
        if (digitNumber(N) == 2) {
            S = ll3s[osto] + S;
            S = " " + S;
        }
        if (digitNumber(N) == 1) {
            S = ll2s[osto] + S;
        }
        N = N / 10;
    }
    return S;
}

fun makeThreeDigitsStr(n: Int, flag: Boolean): String {
    if (digitNumber(n) == 2)
        return makeTwoDigitsStr(n, flag);
    if (digitNumber(n) == 1)
        return makeOneDigitStr(n, flag);
    var S = "";
    var N = n;
    var ll1s: MutableList<String> = mutableListOf<String>("", "сто", "двести", "триста",
            "четыреста", "пятьсот", "шестьсот",
            "семьсот", "восемьсот", "девятьсот");
    var ll2s: MutableList<String> = mutableListOf<String>("", "десять", "двадцать", "тридцать",
            "сорок", "пятьдесят", "шестьдесят",
            "семьдесят", "восемьдесят", "девяносто");
    var ll3s: MutableList<String> = mutableListOf<String>("", "один", "два", "три",
            "четыре", "пять", "шесть",
            "семь", "восемь", "девять",
            "десять", "одиннадцать", "двенадцать",
            "тринадцать", "четырнадцать", "пятнадцать",
            "шестнадцать", "семнадцать", "восемьнадцать",
            "девятнадцать");
    if (flag)
    {
    	ll3s[1] = "одна";
    	ll3s[2] = "две"
    } 
    while (N > 0) {
        var osto = N % 10;
        if (digitNumber(N) == 3) {
            if ((N / 10) % 10 == 1) {
                S = ll3s[N % 100] + S;
                S = " " + S;
                N = N / 100;
                osto = N % 10;
            } else {
                S = ll3s[osto] + S;
                S = " " + S;
            }
        }
        if (digitNumber(N) == 2) {
            S = ll2s[osto] + S;
            S = " " + S;
        }
        if (digitNumber(N) == 1) {
            S = ll1s[osto] + S;

        }
        N = N / 10;
    }
    return S;
}

fun russian(n: Int): String {
    if (n == 0) {
        return "ноль";
    }
    var numstring = java.lang.String.valueOf(n);
    if (digitNumber(n) < 4) {
        val S = makeThreeDigitsStr(n, false);
        return S.replace("\\s+".toRegex(), " ");
    }
    var nump2 = n / 1000;
    var nump1 = n % 1000;
    var Sp2 = makeThreeDigitsStr(nump2, true);
    var Sp1 = makeThreeDigitsStr(nump1, false);
    var tn1:Int =-1; /* number of thousands */
    var tn2:Int=-1;
    tn1 = numstring[numstring.length - 4].toString().toInt();
    if (digitNumber(n) > 4)
    	tn2 = (numstring[numstring.length - 5].toString() + numstring[numstring.length - 4].toString()).toInt();
    
    if (tn2 > 10 && tn2 < 20)
    {
        Sp2 += " тысяч ";
        var Sret = Sp2 + Sp1;
    	return (Sret.replace("\\s+".toRegex(), " ")).replace("(^ *)|( *?$)".toRegex(), "");
    }
    
    if (tn1 == 1) {
        Sp2 += " тысяча ";
    }
    if (tn1 > 1 && tn1 < 5) {
        Sp2 += " тысячи ";
    }
    if (tn1 >= 5 || tn1 == 0) {
         Sp2 += " тысяч ";
    }
    var Sret = Sp2 + Sp1;

    return (Sret.replace("\\s+".toRegex(), " ")).replace("(^ *)|( *?$)".toRegex(), "");
}
