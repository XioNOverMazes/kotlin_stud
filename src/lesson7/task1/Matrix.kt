@file:Suppress("UNUSED_PARAMETER", "unused")

package lesson7.task1

/**
 * Ячейка матрицы: row = ряд, column = колонка
 */
data class Cell(val row: Int, val column: Int)

/*
  Funcs out from Matrix<E> interface
*/
fun <E> findCell(m: Matrix<E>, value: E): Cell {
    var i: Int = 0;
    var j: Int = 0;
    var height = m.height;
    var width = m.width;
    while (i < height) {
        j = 0;
        while (j < width) {
            if (m.get(i, j) == value)
                return Cell(i, j);
            j++;
        }
        i++;
    }
    return Cell(-1, -1);
}

fun <E> copy(to: Matrix<E>, from: Matrix<E>) {
    var i: Int = 0;
    var j: Int = 0;
    var height = to.height;
    var width = to.width;
    while (i < height) {
        j = 0;
        while (j < width) {
            to.set(i, j, from.get(i, j)!!);
            j++;
        }
        i++;
    }
}
/*
  End
*/

/**
 * Интерфейс, описывающий возможности матрицы. E = тип элемента матрицы
 */
interface Matrix<E> {
    /** Высота */
    val height: Int


    /** Ширина */
    val width: Int


    /**
     * Доступ к ячейке.
     * Методы могут бросить исключение, если ячейка не существует или пуста
     */
    operator fun get(row: Int, column: Int): E

    operator fun get(cell: Cell): E

    /**
     * Запись в ячейку.
     * Методы могут бросить исключение, если ячейка не существует
     */
    operator fun set(row: Int, column: Int, value: E)

    operator fun set(cell: Cell, value: E)

}

/**
 * Простая
 *
 * Метод для создания матрицы, должен вернуть РЕАЛИЗАЦИЮ Matrix<E>.
 * height = высота, width = ширина, e = чем заполнить элементы.
 * Бросить исключение IllegalArgumentException, если height или width <= 0.
 */
fun <E> createMatrix(height: Int, width: Int, e: E): Matrix<E> {
    if (height <= 0 || width <= 0)
        throw IllegalArgumentException("height <= 0 || width <= 0");
    val mat = MatrixImpl<E>(height, width);
    mat.fillvals(height, width, e);
    return mat;
}


/**
 * Средняя сложность
 *
 * Реализация интерфейса "матрица"
 */
class MatrixImpl<E>(override val height: Int, override val width: Int) : Matrix<E> {
    private val mat = mutableMapOf<Cell, E>()


    override fun get(row: Int, column: Int): E {
        val c = Cell(row, column);
        if (!(c.row > height - 1 || c.column > width - 1 || c.row < 0 || c.column < 0))
            return mat[c]!!;
        throw IllegalStateException("bad get matrix boiii!");
    }

    override fun get(cell: Cell): E {
        if (!(cell.row > height - 1 || cell.column > width - 1 || cell.row < 0 || cell.column < 0))
            return mat[cell]!!;
        throw IllegalStateException("bad get matrix boiii!");
    }


    override fun set(row: Int, column: Int, value: E) {
        val c = Cell(row, column);
        if (!(c.row > height - 1 || c.column > width - 1 || c.row < 0 || c.column < 0))
            mat[c] = value;
    }

    override fun set(cell: Cell, value: E) {
        if (!(cell.row > height - 1 || cell.column > width - 1 || cell.row < 0 || cell.column < 0))
            mat[cell] = value;
    }


    fun fillvals(row: Int, column: Int, value: E) {
        var i: Int = 0;
        var j: Int = 0;
        while (i < row) {
            j = 0;
            while (j < column) {
                set(i, j, value);
                j += 1;
            }
            i += 1;
        }
    }

    override fun equals(other: Any?): Boolean = other is MatrixImpl<*> &&
            height == other.height &&
            width == other.width &&
            mat == other.mat;


    override fun toString(): String {
        var s: String = "";
        var i: Int = 0;
        var j: Int = 0;
        while (i < height) {
            j = 0;
            while (j < width) {
                s += get(i, j).toString() + " ";
                j += 1;
            }
            s += "\n";
            i += 1;
        }
        return s;
    }
}
