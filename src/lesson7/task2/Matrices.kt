@file:Suppress("UNUSED_PARAMETER")

package lesson7.task2

import lesson7.task1.Matrix
import lesson7.task1.Cell
import lesson7.task1.createMatrix
import lesson7.task1.copy
import lesson7.task1.findCell

// Все задачи в этом файле требуют наличия реализации интерфейса "Матрица" в Matrix.kt

/**
 * Пример
 *
 * Транспонировать заданную матрицу matrix.
 * При транспонировании строки матрицы становятся столбцами и наоборот:
 *
 * 1 2 3      1 4 6 3
 * 4 5 6  ==> 2 5 5 2
 * 6 5 4      3 6 4 1
 * 3 2 1
 */
var shelo: String = "hello";

fun <E> transpose(matrix: Matrix<E>): Matrix<E> {
    if (matrix.width < 1 || matrix.height < 1) return matrix
    var result = createMatrix(matrix.width, matrix.height, matrix.get(0, 0));
    for (i in 0 until matrix.width) {
        for (j in 0 until matrix.height) {
            result.set(i, j, matrix.get(j, i));
        }
    }
    return result;
}

/**
 * Пример
 *
 * Сложить две заданные матрицы друг с другом.
 * Складывать можно только матрицы совпадающего размера -- в противном случае бросить IllegalArgumentException.
 * При сложении попарно складываются соответствующие элементы матриц
 */
operator fun Matrix<Int>.plus(other: Matrix<Int>): Matrix<Int> {
    if (width != other.width || height != other.height) throw IllegalArgumentException()
    if (width < 1 || height < 1) return this
    val result = createMatrix(other.height, other.width, other.get(0, 0))
    for (i in 0 until height) {
        for (j in 0 until width) {
            result.set(i, j, get(i, j) + other.get(i, j));
        }
    }
    return result;
}

/**
 * Сложная
 *
 * Заполнить матрицу заданной высоты height и ширины width
 * натуральными числами от 1 до m*n по спирали,
 * начинающейся в левом верхнем углу и закрученной по часовой стрелке.
 *
 * Пример для height = 3, width = 4:
 *  1  2  3  4
 * 10 11 12  5
 *  9  8  7  6
 */
fun generateSpiral(height: Int, width: Int): Matrix<Int> {
    var mat = createMatrix(height, width, 0);
    var curCnt = 1;
    var xdist = width;
    var ydist = height - 1;
    var xpos = 0;
    var ypos = 0;
    var i = 0;
    var j = 0;
    /*
     *	dir 0 = right
     * 	dir 1 = down
     * 	dir 2 = left
     * 	dir 3 = up
     */
    var dir = 0;
    var colCnt = height * width;
    while (true) {
        /* right */
        if (dir == 0) {
            var destx = xpos + xdist;
            while (xpos != destx - 1) {
                mat.set(ypos, xpos, curCnt);
                curCnt += 1;
                xpos += 1;
            }
            mat.set(ypos, xpos, curCnt);
            curCnt += 1;
            ypos += 1;
            dir = 1;
            xdist -= 1;
            if (curCnt > colCnt)
                break;
        }
        /* down */
        if (dir == 1) {
            var desty = ypos + ydist;
            while (ypos != desty - 1) {
                mat.set(ypos, xpos, curCnt);
                curCnt += 1;
                ypos += 1;
            }
            mat.set(ypos, xpos, curCnt);
            curCnt += 1;
            xpos -= 1;
            dir = 2;
            ydist -= 1;
            if (curCnt > colCnt)
                break;
        }
        /* left */
        if (dir == 2) {
            var destx = xpos - xdist;
            while (xpos != destx + 1) {
                mat.set(ypos, xpos, curCnt);
                curCnt += 1;
                xpos -= 1;

            }
            mat.set(ypos, xpos, curCnt);
            curCnt += 1;
            ypos -= 1;
            dir = 3;
            xdist -= 1;
            if (curCnt > colCnt)
                break;
        }
        /* up */

        if (dir == 3) {
            var desty = ypos - ydist;
            while (ypos != desty + 1) {

                mat.set(ypos, xpos, curCnt);
                curCnt += 1;
                ypos -= 1;
            }
            mat.set(ypos, xpos, curCnt);
            curCnt += 1;
            xpos += 1;
            dir = 0;
            ydist -= 1;
            if (curCnt > colCnt)
                break;
        }

    }
    return mat;
}

/**
 * Сложная
 *
 * Заполнить матрицу заданной высоты height и ширины width следующим образом.
 * Элементам, находящимся на периферии (по периметру матрицы), присвоить значение 1;
 * периметру оставшейся подматрицы – значение 2 и так далее до заполнения всей матрицы.
 *
 * Пример для height = 5, width = 6:
 *  1  1  1  1  1  1
 *  1  2  2  2  2  1
 *  1  2  3  3  2  1
 *  1  2  2  2  2  1
 *  1  1  1  1  1  1
 */

fun generateRectangles(height: Int, width: Int): Matrix<Int> {
    var mat = createMatrix(height, width, 0);
    var dif_width = width;
    var dif_height = height;
    var i = 0;
    var j = 0;
    var cyclvl = 0;
    while (dif_width > 0 && dif_height > 0) {
        if (dif_width == 1) {
            i = 0;
            while (i < dif_height) {
                mat.set(i + cyclvl, cyclvl, cyclvl + 1);
                i += 1;
            }
            break;
        }
        if (dif_height == 1) {
            i = 0;
            while (i < dif_width) {
                mat.set(cyclvl, i + cyclvl, cyclvl + 1);
                i += 1;
            }
            break;
        }
        i = 0;
        while (i < dif_width) {
            mat.set(cyclvl, i + cyclvl, cyclvl + 1);
            mat.set(cyclvl + dif_height - 1, i + cyclvl, cyclvl + 1);
            i += 1;
        }
        i = 0;
        while (i < dif_height) {
            mat.set(i + cyclvl, cyclvl, cyclvl + 1);
            mat.set(i + cyclvl, cyclvl + dif_width - 1, cyclvl + 1);
            i += 1;
        }
        cyclvl += 1;
        dif_width -= 2;
        dif_height -= 2;

    }
    return mat;
}


/**
 * Сложная
 *
 * Заполнить матрицу заданной высоты height и ширины width диагональной змейкой:
 * в левый верхний угол 1, во вторую от угла диагональ 2 и 3 сверху вниз, в третью 4-6 сверху вниз и так далее.
 *
 * Пример для height = 5, width = 4:
 *  1  2  4  7
 *  3  5  8 11
 *  6  9 12 15
 * 10 13 16 18
 * 14 17 19 20
 */
fun generateSnake(height: Int, width: Int): Matrix<Int> {
    var curCnt = 1;
    var i = 0;
    var mat = createMatrix(height, width, 0);
    /* for rows */
    while (i < width) {
        var x = i;
        var y = 0;
        /* fill diagonal */
        while (true) {
            mat.set(y, x, curCnt);
            curCnt += 1;
            x -= 1;
            y += 1;
            if (y > i || y == height)
                break;
        }
        i += 1;
    }
    i = 1;
    while (i < height) {
        var x = width - 1;
        var y = i;
        /* fill diyagonaele */
        while (true) {
            mat.set(y, x, curCnt);
            curCnt += 1;
            x -= 1;
            y += 1;
            if (x < 0 || y > height - 1)
                break;
        }
        i += 1;
    }
    return mat;
}


/**
 * Средняя
 *
 * Содержимое квадратной матрицы matrix (с произвольным содержимым) повернуть на 90 градусов по часовой стрелке.
 * Если height != width, бросить IllegalArgumentException.
 *
 * Пример:    Станет:
 * 1 2 3      7 4 1
 * 4 5 6      8 5 2
 * 7 8 9      9 6 3
 */
fun <E> rotate(matrix: Matrix<E>): Matrix<E> = TODO()

/**
 * Сложная
 *
 * Проверить, является ли квадратная целочисленная матрица matrix латинским квадратом.
 * Латинским квадратом называется матрица размером n x n,
 * каждая строка и каждый столбец которой содержат все числа от 1 до n.
 * Если height != width, вернуть false.
 *
 * Пример латинского квадрата 3х3:
 * 2 3 1
 * 1 2 3
 * 3 1 2
 */
fun isLatinSquare(matrix: Matrix<Int>): Boolean = TODO()

/**
 * Средняя
 *
 * В матрице matrix каждый элемент заменить суммой непосредственно примыкающих к нему
 * элементов по вертикали, горизонтали и диагоналям.
 *
 * Пример для матрицы 4 x 3: (11=2+4+5, 19=1+3+4+5+6, ...)
 * 1 2 3       11 19 13
 * 4 5 6  ===> 19 31 19
 * 6 5 4       19 31 19
 * 3 2 1       13 19 11
 *
 * Поскольку в матрице 1 х 1 примыкающие элементы отсутствуют,
 * для неё следует вернуть как результат нулевую матрицу:
 *
 * 42 ===> 0
 */
fun sumNeighbours(matrix: Matrix<Int>): Matrix<Int> = TODO()

/**
 * Средняя
 *
 * Целочисленная матрица matrix состоит из "дырок" (на их месте стоит 0) и "кирпичей" (на их месте стоит 1).
 * Найти в этой матрице все ряды и колонки, целиком состоящие из "дырок".
 * Результат вернуть в виде Holes(rows = список дырчатых рядов, columns = список дырчатых колонок).
 * Ряды и колонки нумеруются с нуля. Любой из спискоов rows / columns может оказаться пустым.
 *
 * Пример для матрицы 5 х 4:
 * 1 0 1 0
 * 0 0 1 0
 * 1 0 0 0 ==> результат: Holes(rows = listOf(4), columns = listOf(1, 3)): 4-й ряд, 1-я и 3-я колонки
 * 0 0 1 0
 * 0 0 0 0
 */
fun findHoles(matrix: Matrix<Int>): Holes = TODO()

/**
 * Класс для описания местонахождения "дырок" в матрице
 */
data class Holes(val rows: List<Int>, val columns: List<Int>)

/**
 * Средняя
 *
 * В целочисленной матрице matrix каждый элемент заменить суммой элементов подматрицы,
 * расположенной в левом верхнем углу матрицы matrix и ограниченной справа-снизу данным элементом.
 *
 * Пример для матрицы 3 х 3:
 *
 * 1  2  3      1  3  6
 * 4  5  6  =>  5 12 21
 * 7  8  9     12 27 45
 *
 * К примеру, центральный элемент 12 = 1 + 2 + 4 + 5, элемент в левом нижнем углу 12 = 1 + 4 + 7 и так далее.
 */
fun sumSubMatrix(matrix: Matrix<Int>): Matrix<Int> = TODO()

/**
 * Сложная
 *
 * Даны мозаичные изображения замочной скважины и ключа. Пройдет ли ключ в скважину?
 * То есть даны две матрицы key и lock, key.height <= lock.height, key.width <= lock.width, состоящие из нулей и единиц.
 *
 * Проверить, можно ли наложить матрицу key на матрицу lock (без поворота, разрешается только сдвиг) так,
 * чтобы каждой единице в матрице lock (штырь) соответствовал ноль в матрице key (прорезь),
 * а каждому нулю в матрице lock (дырка) соответствовала, наоборот, единица в матрице key (штырь).
 * Ключ при сдвиге не может выходить за пределы замка.
 *
 * Пример: ключ подойдёт, если его сдвинуть на 1 по ширине
 * lock    key
 * 1 0 1   1 0
 * 0 1 0   0 1
 * 1 1 1
 *
 * Вернуть тройку (Triple) -- (да/нет, требуемый сдвиг по высоте, требуемый сдвиг по ширине).
 * Если наложение невозможно, то первый элемент тройки "нет" и сдвиги могут быть любыми.
 */
fun canOpenLock(key: Matrix<Int>, lock: Matrix<Int>): Triple<Boolean, Int, Int> = TODO()

/**
 * Простая
 *
 * Инвертировать заданную матрицу.
 * При инвертировании знак каждого элемента матрицы следует заменить на обратный
 */
operator fun Matrix<Int>.unaryMinus(): Matrix<Int> = TODO(this.toString())

/**
 * Средняя
 *
 * Перемножить две заданные матрицы друг с другом.
 * Матрицы можно умножать, только если ширина первой матрицы совпадает с высотой второй матрицы.
 * В противном случае бросить IllegalArgumentException.
 * Подробно про порядок умножения см. статью Википедии "Умножение матриц".
 */
operator fun Matrix<Int>.times(other: Matrix<Int>): Matrix<Int> = TODO(this.toString())

/**
 * Сложная
 *
 * В матрице matrix размером 4х4 дана исходная позиция для игры в 15, например
 *  5  7  9  1
 *  2 12 14 15
 *  3  4  6  8
 * 10 11 13  0
 *
 * Здесь 0 обозначает пустую клетку, а 1-15 – фишки с соответствующими номерами.
 * Напомним, что "игра в 15" имеет квадратное поле 4х4, по которому двигается 15 фишек,
 * одна клетка всегда остаётся пустой. Цель игры -- упорядочить фишки на игровом поле.
 *
 * В списке moves задана последовательность ходов, например [8, 6, 13, 11, 10, 3].
 * Ход задаётся номером фишки, которая передвигается на пустое место (то есть, меняется местами с нулём).
 * Фишка должна примыкать к пустому месту по горизонтали или вертикали, иначе ход не будет возможным.
 * Все номера должны быть в пределах от 1 до 15.
 * Определить финальную позицию после выполнения всех ходов и вернуть её.
 * Если какой-либо ход является невозможным или список содержит неверные номера,
 * бросить IllegalStateException.
 *
 * В данном случае должно получиться
 * 5  7  9  1
 * 2 12 14 15
 * 0  4 13  6
 * 3 10 11  8
 */
fun fifteenGameFindPoint(matrix: Matrix<Int>, point: Int): Cell {
    var i = 0;
    var j = 0;
    while (i < matrix.height) {
        j = 0;
        while (j < matrix.width) {
            if (matrix.get(i, j) == point)
                return Cell(i, j);
            j += 1;
        }
        i += 1;
    }
    return Cell(-1, -1);
}

fun fifteenGameMakeMove(matrix: Matrix<Int>, point: Int): Unit {
    var noll_cell = fifteenGameFindPoint(matrix, 0);
    var pt_cell = fifteenGameFindPoint(matrix, point);

    var dist = Math.abs(pt_cell.row - noll_cell.row) + Math.abs(pt_cell.column - noll_cell.column);

    if (dist != 1)
        IllegalStateException("bad move boiiii!!!");

    if (noll_cell.row == -1)
        throw IllegalStateException("bad point");
    if (pt_cell.row == -1)
        throw IllegalStateException("bad point");
    matrix.set(noll_cell, matrix.get(pt_cell)!!);
    matrix.set(pt_cell, 0);
}

fun fifteenGameMoves(matrix: Matrix<Int>, moves: List<Int>): Matrix<Int> {
    for (i: Int in moves) {
        if (i > 15 || i < 1)
            IllegalStateException("bad move boiiii!!!");

        fifteenGameMakeMove(matrix, i);
    }
    return matrix;
}


/**
 * Очень сложная
 *
 * В матрице matrix размером 4х4 дана исходная позиция для игры в 15, например
 *  5  7  9  2
 *  1 12 14 15
 *  3  4  6  8
 * 10 11 13  0
 *
 * Здесь 0 обозначает пустую клетку, а 1-15 – фишки с соответствующими номерами.
 * Напомним, что "игра в 15" имеет квадратное поле 4х4, по которому двигается 15 фишек,
 * одна клетка всегда остаётся пустой.
 *
 * Цель игры -- упорядочить фишки на игровом поле, приведя позицию к одному из следующих двух состояний:
 *
 *  1  2  3  4          1  2  3  4
 *  5  6  7  8   ИЛИ    5  6  7  8
 *  9 10 11 12          9 10 11 12
 * 13 14 15  0         13 15 14  0
 *
 * Можно математически доказать, что РОВНО ОДНО из этих двух состояний достижимо из любой исходной позиции.
 *
 * Вернуть решение -- список ходов, приводящих исходную позицию к одной из двух упорядоченных.
 * Каждый ход -- это перемена мест фишки с заданным номером с пустой клеткой (0),
 * при этом заданная фишка должна по горизонтали или по вертикали примыкать к пустой клетке (но НЕ по диагонали).
 * К примеру, ход 13 в исходной позиции меняет местами 13 и 0, а ход 11 в той же позиции невозможен.
 *
 * Одно из решений исходной позиции:
 *
 * [8, 6, 14, 12, 4, 11, 13, 14, 12, 4,
 * 7, 5, 1, 3, 11, 7, 3, 11, 7, 12, 6,
 * 15, 4, 9, 2, 4, 9, 3, 5, 2, 3, 9,
 * 15, 8, 14, 13, 12, 7, 11, 5, 7, 6,
 * 9, 15, 8, 14, 13, 9, 15, 7, 6, 12,
 * 9, 13, 14, 15, 12, 11, 10, 9, 13, 14,
 * 15, 12, 11, 10, 9, 13, 14, 15]
 *
 * Перед решением этой задачи НЕОБХОДИМО решить предыдущую
 */

/*
  До изменения интерфейса Matrix<E> работало. Искало решения у меня максимум за около 60 секунд
*/

var termarr1 = listOf(listOf(1, 2, 3, 4),
        listOf(5, 6, 7, 8),
        listOf(9, 10, 11, 12),
        listOf(13, 14, 15, 0));
var termarr2 = listOf(listOf(1, 2, 3, 4),
        listOf(5, 6, 7, 8),
        listOf(9, 10, 11, 12),
        listOf(13, 15, 14, 0));
var testarr1 = listOf(listOf(0, 1, 2, 3),
        listOf(4, 5, 6, 7),
        listOf(8, 9, 10, 11),
        listOf(12, 13, 14, 15));
var termmat1: Matrix<Int> = createMatrix4x4(termarr1);
var termmat2: Matrix<Int> = createMatrix4x4(termarr2);
/* Learning Coefficient
   umnozhaem evristiku na eto delo */
var LEARN_CF = 30;

fun createMatrix4x4(dat: List<List<Int>>): Matrix<Int> {
    var i = 0;
    var j = 0;
    var mat = createMatrix(4, 4, 0);
    while (i < 4) {
        while (j < 4) {
            mat.set(i, j, dat[i][j]);
            j++;
        }
        j = 0;
        i++
    }
    return mat;
}

class State(mat: Matrix<Int>) {
    var matrix = mat;
    private
    var gmetric = 0;
    private
    var hmetric = 0;
    private
    var parent: State? = null;

    fun setGMetric(newval: Int) {
        gmetric = newval; }

    fun setHMetric(newval: Int) {
        hmetric = newval; }

    fun getAllMetric(): Int {
        return gmetric + hmetric; }

    fun getGMetric(): Int {
        return gmetric; }

    fun getHMetric(): Int {
        return hmetric; }

    fun setParent(s: State?) {
        parent = s;
    }

    fun getParent(): State? {
        return parent; }
}

fun makeMove(srcmat: Matrix<Int>, fval: Int) {
    var noll = findCell(srcmat, 0);
    var valc = findCell(srcmat, fval);
    srcmat.set(noll.row, noll.column, fval);
    srcmat.set(valc.row, valc.column, fval);
}

fun makeMove(srcmat: Matrix<Int>, row: Int, col: Int) {
    var c = Cell(row, col);
    if (c.row < 4 && c.column < 4 && c.row > -1 && c.column > -1) {
        var noll = findCell(srcmat, 0);
        srcmat.set(noll.row, noll.column, srcmat.get(row, col));
        srcmat.set(row, col, 0);
    }
}

fun createCopy(srcmat: Matrix<Int>): Matrix<Int> {
    val newmat = createMatrix<Int>(srcmat.height, srcmat.width, 0);
    copy(newmat, srcmat);
    return newmat;
}

fun getPossibleWays(srcmat: Matrix<Int>): List<Matrix<Int>> {
    var ret = mutableListOf<Matrix<Int>>();
    var tmp: Matrix<Int> = createMatrix(4, 4, 0);
    copy(tmp, srcmat);
    var noll = findCell(srcmat, 0);

    makeMove(tmp, noll.row - 1, noll.column);
    if (tmp != srcmat)
        ret.add(createCopy(tmp));
    copy(tmp, srcmat);

    makeMove(tmp, noll.row + 1, noll.column);
    if (tmp != srcmat)
        ret.add(createCopy(tmp));
    copy(tmp, srcmat);

    makeMove(tmp, noll.row, noll.column + 1);
    if (tmp != srcmat)
        ret.add(createCopy(tmp));
    copy(tmp, srcmat);

    makeMove(tmp, noll.row, noll.column - 1);
    if (tmp != srcmat)
        ret.add(createCopy(tmp));
    copy(tmp, srcmat);
    return ret;
}

fun calcHMetric(mat: Matrix<Int>, stage: Int): Int {
    var metric: Int = 0;
    var lin_c: Int = 0;
    var manhattan: Int = 0;
    /* stage 14 - 15 can be changed */
    var termat = createMatrix4x4(termarr1);

    var i = 0;
    var j = 0;

    while (i < 4) {
        j = 0;
        while (j < 4) {
            var curnum = mat.get(i, j);

            var pointc = findCell(termat, curnum);
            manhattan +=
                    LEARN_CF * (Math.abs(pointc.row - i) + Math.abs(pointc.column - j));
            /* For linear conflict */
            if (i == pointc.row) {
                var lin_i = 0;
                while (lin_i < 4) {
                    var lin_curnum = mat.get(i, lin_i);
                    var lin_pointc = findCell(termat, lin_curnum);
                    if (i == lin_pointc.row) {
                        if (pointc.column > j && lin_pointc.column < lin_i) {
                            lin_c += LEARN_CF * 2;
                        }
                    }
                    lin_i += 1;
                }
            }
            if (j == pointc.column) {
                var lin_i = 0;
                while (lin_i < 4) {
                    var lin_curnum = mat.get(lin_i, i);
                    var lin_pointc = findCell(termat, lin_curnum);
                    if (j == lin_pointc.column) {
                        if (pointc.row > j && lin_pointc.row < lin_i) {
                            lin_c += LEARN_CF * 2;
                        }
                    }
                    lin_i += 1;
                }
            }
            j += 1;
        }
        i += 1;
    }
    metric += lin_c;
    metric += manhattan;
    /* usaem 3 stadii otdelyaem chto bi shel po pravilnomu turbo-puti */
    if (stage == 0)
        metric += 10000000;
    if (stage == 1)
        metric += 500000;
    if (stage == 2)
        metric += 250000;
    if (stage == 3)
        metric += 0;
    return metric;
}

/* sd newer state*/
fun whatMove(s1: State, s2: State): Int {
    var i = 0;
    var j = 0;
    while (i < 4) {
        j = 0;
        while (j < 4) {
            if (s1.matrix.get(i, j) != s2.matrix.get(i, j)) {
                if (s2.matrix.get(i, j) != 0)
                    return s2.matrix.get(i, j);
            }
            j += 1;
        }
        i += 1;
    }
    return -1;
}

fun generatePath(finish: State): List<State> {
    var path = mutableListOf<State>();
    var s: State? = finish;
    while (s != null) {
        path.add(s);
        s = s.getParent();
    }
    return path;
}

fun chooseMinState(open: List<State>): State? {
    var res: State? = null;
    var min_weight = 1474836470;
    for (i in open) {
        if (i.getAllMetric() < min_weight) {
            min_weight = i.getAllMetric();
            res = i;
        }
    }
    return res;
}

fun generateNextStates(current: State): List<State> {
    var res = mutableListOf<State>();
    var newmatrxs = getPossibleWays(current.matrix);
    for (i in newmatrxs) {
        res.add(State(i));
    }
    return res;
}

/* 0 - 0stage nothing good boi        */
/* 1 - 1stage (1234/xxxx/xxxx/xxxx    */
/* 2 - 2stage (1234/5678/xxxx/xxxx    */
/* 3 - 3stage ( +9 +13 for prev)      */
/* 4 - 4stage got it!!! */

fun isReady(x: State): Int {
    var stage_1 = true;
    var stage_2 = true;
    if (x.matrix == termmat1 || x.matrix == termmat2)
        return 4;

    var i = 0;

    while (i < 4) {
        if (x.matrix.get(0, i) != termarr1[0][i]) {
            stage_1 = false;
            stage_2 = false;
            break;
        }
        if (x.matrix.get(1, i) != termarr1[1][i])
            stage_2 = false;
        i += 1;
    }
    if (stage_1 && stage_2) {
        if (x.matrix.get(0, 2) == 9 && x.matrix.get(0, 3) == 13)
            return 3;
        return 2;
    }
    if (stage_1)
        return 1;
    return 0;
}

fun removeStateFromList(s: State, open: MutableList<State>) {
    var i = 0;
    while (i < open.size) {
        if (s.matrix == open[i].matrix)
            open.removeAt(i);
        i += 1;
    }
}

fun containStateFromList(s: State, list: MutableList<State>): Boolean {
    for (i in list) {
        if (s.matrix == i.matrix)
            return true;
    }
    return false;
}

fun searchSollution(startpoint: State): List<State> {
    var x: State?;
    var close = mutableListOf<State>();
    var open = mutableListOf<State>();
    startpoint.setGMetric(0);
    startpoint.setParent(null);
    startpoint.setHMetric(calcHMetric(startpoint.matrix, 0));
    open.add(startpoint);
    while (!open.isEmpty()) {
        x = chooseMinState(open);
        if (x == null)
            break;
        var stagestatus = isReady(x);
        if (stagestatus == 4) {
            return generatePath(x);
        }

        removeStateFromList(x, open);
        close.add(x);

        var neighbors = generateNextStates(x);
        for (n in neighbors) {
            if (containStateFromList(n, close))
                continue;
            var g = x.getGMetric() + 5;
            if (!containStateFromList(n, open)) {
                var stagestatusForN = isReady(n);
                n.setHMetric(calcHMetric(n.matrix, stagestatusForN));
                n.setParent(x);
                n.setGMetric(g);
                open.add(n);
            }
        }
    }
    return open;
}

fun fifteenGameSolution(matrix: Matrix<Int>): List<Int> {
    if (matrix.width != 4 || matrix.height != 4)
        IllegalStateException("bad boiiii!!!");
    var s_start: State = State(matrix);
    var answer: List<State> = searchSollution(s_start);
    var intanswer = mutableListOf<Int>();
    var i = 1;
    while (i < answer.size) {
        intanswer.add(whatMove(answer[i - 1], answer[i]));
        i++;
    }
    intanswer.reverse();
    return intanswer;
}
