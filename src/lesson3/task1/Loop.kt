@file:Suppress("UNUSED_PARAMETER")

package lesson3.task1

import lesson1.task1.sqr

/**
 * Пример
 *
 * Вычисление факториала
 */
fun factorial(n: Int): Double {
    if (n < 0)
        return 0.0;
    if (n == 0)
        return 1.0;
    else
        return n * factorial(n - 1);
}

/**
 * Пример
 *
 * Проверка числа на простоту -- результат true, если число простое
 */
fun isPrime(n: Int): Boolean {
    if (n < 2) return false
    for (m in 2..Math.sqrt(n.toDouble()).toInt()) {
        if (n % m == 0) return false
    }
    return true;
}

/**
 * Пример
 *
 * Проверка числа на совершенность -- результат true, если число совершенное
 */
fun isPerfect(n: Int): Boolean {
    var sum = 1
    for (m in 2..n / 2) {
        if (n % m > 0) continue
        sum += m
        if (sum > n) break
    }
    return sum == n
}

/**
 * Пример
 *
 * Найти число вхождений цифры m в число n
 */
fun digitCountInNumber(n: Int, m: Int): Int =
        when {
            n == m -> 1
            n < 10 -> 0
            else -> digitCountInNumber(n / 10, m) + digitCountInNumber(n % 10, m)
        }

/**
 * Тривиальная
 *
 * Найти количество цифр в заданном числе n.
 * Например, число 1 содержит 1 цифру, 456 -- 3 цифры, 65536 -- 5 цифр.
 */
fun digitNumber(n: Int): Int {
    if (n == 0) return 1;
	     var nn: Int = n;
       var cnt: Int = 0;
    while (nn != 0)
    {
	   nn = nn/10;
	cnt++;
    }
    return cnt;
}

/**
 * Простая
 *
 * Найти число Фибоначчи из ряда 1, 1, 2, 3, 5, 8, 13, 21, ... с номером n.
 * Ряд Фибоначчи определён следующим образом: fib(1) = 1, fib(2) = 1, fib(n+2) = fib(n) + fib(n+1)
 */
fun fib(n: Int): Int {
    if (n == 1 || n == 2) {
        return 1;
    }
    var x1: Int = 1;
    var x2: Int = 1;
    var tmp: Int = 0;
    var i: Int = 0;
    while (i != n - 2) {
        tmp = x1 + x2;
        x1 = x2;
        x2 = tmp;
        i++;
    }
    return x2;
}

/**
 * Простая
 *
 * Для заданных чисел m и n найти наименьшее общее кратное, то есть,
 * минимальное число k, которое делится и на m и на n без остатка
 */
fun lcm(m: Int, n: Int): Int {

    var cnt: Int = m * n - 1;
    var i: Int = Math.max(n, m);
    while (i <= cnt) {
        if (((i % n) == 0) && ((i % m) == 0)) {
            return i;
        }
        i++;
    }
    return n * m;
}

/**
 * Простая
 *
 * Для заданного числа n > 1 найти минимальный делитель, превышающий 1
 */
fun minDivisor(n: Int): Int {
    var i: Int = 2;
    while (i <= n) {
        if ((n % i) == 0) return i;
        i++;
    }
    return -1;
}

/**
 * Простая
 *
 * Для заданного числа n > 1 найти максимальный делитель, меньший n
 */
fun maxDivisor(n: Int): Int {
    var i: Int = n - 1;
    while (i >= 1) {
        if ((n % i) == 0) {
            return i;
        }
        i--;
    }
    return -1;
}

/**
 * Простая
 *
 * Определить, являются ли два заданных числа m и n взаимно простыми.
 * Взаимно простые числа не имеют общих делителей, кроме 1.
 * Например, 25 и 49 взаимно простые, а 6 и 8 -- нет.
 */
fun isCoPrime(m: Int, n: Int): Boolean {
    var i: Int = Math.min(m, n);
    while (i != 1) {
        if (((m % i) == 0) && ((n % i) == 0)) {
            return false;
        }
        i--;
    }
    return true;
}

/**
 * Простая
 *
 * Для заданных чисел m и n, m <= n, определить, имеется ли хотя бы один точный квадрат между m и n,
 * то есть, существует ли такое целое k, что m <= k*k <= n.
 * Например, для интервала 21..28 21 <= 5*5 <= 28, а для интервала 51..61 квадрата не существует.
 */
fun squareBetweenExists(m: Int, n: Int): Boolean {
    if (m == n) return true;
    var first_sq: Double = Math.sqrt(Math.min(m, n).toDouble());
    var sec_sq: Double = Math.sqrt(Math.max(m, n).toDouble());
    if ((sec_sq.toInt() - first_sq.toInt()) > 0)
        return true;
    return false;
}

/**
 * Средняя
 *
 * Для заданного x рассчитать с заданной точностью eps
 * sin(x) = x - x^3 / 3! + x^5 / 5! - x^7 / 7! + ...
 * Нужную точность считать достигнутой, если очередной член ряда меньше eps по модулю
 */
fun sin(x: Double, eps: Double): Double {
    var r_x: Double = x;
    while (r_x > 2 * Math.PI) {
        r_x -= 2 * Math.PI;
    }
    var sum: Double = r_x;
    var toadd: Double = 0.0;
    var i: Int = 3;
    var plus: Boolean = false;
    while (true) {
        toadd = (Math.pow(r_x, i.toDouble())) / (factorial(i));
        if (plus)
            sum += toadd;
        else
            sum -= toadd;
        if (toadd < eps) {
            return sum;
        }
        plus = !plus
        i += 2;
    }
    return 0.0;
}

/**
 * Средняя
 *
 * Для заданного x рассчитать с заданной точностью eps
 * cos(x) = 1 - x^2 / 2! + x^4 / 4! - x^6 / 6! + ...
 * Нужную точность считать достигнутой, если очередной член ряда меньше eps по модулю
 */
fun cos(x: Double, eps: Double): Double {
    var r_x: Double = x;
    while (r_x > 2 * Math.PI) {
        r_x -= 2 * Math.PI;
    }
    var sum: Double = 1.0;
    var toadd: Double = 0.0;
    var i: Int = 2;
    var plus: Boolean = false;
    while (true) {
        toadd = (Math.pow(r_x, i.toDouble())) / (factorial(i));
        if (plus)
            sum += toadd;
        else
            sum -= toadd;
        if (toadd < eps) {

            return sum;
        }
        plus = !plus;
        i += 2;
    }
    return 0.0;
}

/**
 * Средняя
 *
 * Поменять порядок цифр заданного числа n на обратный: 13478 -> 87431.
 * Не использовать строки при решении задачи.
 */
fun revert(n: Int): Int {
    var n1: Int = n;
    var n2: Int = 0;
    while (n1 > 0) {
        var dgt: Int = n1 % 10;
        n1 = (n1 / 10).toInt();
        n2 = n2 * 10;
        n2 = n2 + dgt;
    }
    return n2;
}

/**
 * Средняя
 *
 * Проверить, является ли заданное число n палиндромом:
 * первая цифра равна последней, вторая -- предпоследней и так далее.
 * 15751 -- палиндром, 3653 -- нет.
 */
fun isPalindrome(n: Int): Boolean {
    var num: Int = digitNumber(n);
    var center_idx: Int = num / 2;
    var nstr = n.toString();
    var i: Int = 0;
    var j: Int = digitNumber(n) - 1;
    while (i < j) {
        if (nstr[i] != nstr[j]) {
            return false;
        }
        i++;
        j--;
    }
    return true;
}

/**
 * Средняя
 *
 * Для заданного числа n определить, содержит ли оно различающиеся цифры.
 * Например, 54 и 323 состоят из разных цифр, а 111 и 0 из одинаковых.
 */
fun hasDifferentDigits(n: Int): Boolean {
    var strn = n.toString();
    var dign = digitNumber(n);
    var i: Int = 0;
    while (i < dign) {
        if (strn[i] != strn[0]) {
            return true;
        }
        i++
    }
    return false;
}

/*user func */
/* GetDigitFromNum (5476, 2) == 7 */
fun getDigitFromNum(num: Int, idx: Int): Int {
    var strn = num.toString();
    return strn[idx].toString().toInt();
}

/**
 * Сложная
 *
 * Найти n-ю цифру последовательности из квадратов целых чисел:
 * 149162536496481100121144...
 * Например, 2-я цифра равна 4, 7-я 5, 12-я 6.
 */
fun squareSequenceDigit(n: Int): Int {
    var numberofsqrs: Int = 0;
    var i: Int = 1;
    while (true) {
        var square: Int = sqr(i.toDouble()).toInt();
        var digitssqr: Int = digitNumber(square);
        if (numberofsqrs < n && n <= numberofsqrs + digitssqr) {

            var pos: Int = n - numberofsqrs;
            return getDigitFromNum(square, pos - 1);
        }
        numberofsqrs += digitssqr;
        i++;
    }
    return -1;
}

/**
 * Сложная
 *
 * Найти n-ю цифру последовательности из чисел Фибоначчи (см. функцию fib выше):
 * 1123581321345589144...
 * Например, 2-я цифра равна 1, 9-я 2, 14-я 5.
 */
fun fibSequenceDigit(n: Int): Int {
    var numberoffib: Int = 0;
    var i: Int = 1;
    while (true) {
        var fibo: Int = fib(i);
        var digitsfibo: Int = digitNumber(fibo);
        if (numberoffib < n && n <= numberoffib + digitsfibo) {
            var pos: Int = n - numberoffib;
            return getDigitFromNum(fibo, pos - 1);
        }
        numberoffib += digitsfibo;
        i++
    }
    return -1;
}
